<?php

use App\Http\Controllers\KuriController;
use App\Models\Kuri;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('view-kuri', function () {
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $no_of_lots = Kuri::where('table_name', $table)->first(['no_of_lots']);
        $no_of_lots = $no_of_lots->no_of_lots;

        $members_list = DB::table($table)->orderBy('lot_number')->get();
        return view('kuri_view', compact('members_list','no_of_lots'));
    })->name('view-kuri');

    Route::get('reports', function () {
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $no_of_lots = Kuri::where('table_name', $table)->first(['no_of_lots']);
        $no_of_lots = $no_of_lots->no_of_lots;

        $members_list = DB::table($table)->orderBy('lot_number')->get();
        return view('reports', compact('members_list','no_of_lots'));
    })->name('reports');

    Route::get('MakeNewChitti', function () {
        return view('makekuri.make_kuri');
    })->name('make_new_chitti');

    // Route::post('CreateNewChitti', function () {
    //     return view('makekuri.make_kuri');
    // })->name('create_new_chitti');
    Route::post('submit-form', [KuriController::class, 'store'])->name('myFormSubmit');
    Route::post('create-member', [KuriController::class, 'addMember'])->name('memberFormSubmit');
    Route::post('mark-payment', [KuriController::class, 'mark_payment'])->name('mark.payment');
    Route::get('get_reports/{lot_no}', [KuriController::class, 'get_reports'])->name('get.reports');

});

Route::get('logout', function () {
    Auth::logout();
    return redirect('login');
})->middleware('auth')->name('logout');
