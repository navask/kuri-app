<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kuri extends Model
{
    use HasFactory;
    protected $fillable = [
        'userName',
        'password',
        'name',
        'contact',
        'address',
        'no_of_lots',
        'type',
        'roganisor_lot',
        'acceptTerms',
        'one_lot_amt',
        'total_kuri_amt'
    ];
}
