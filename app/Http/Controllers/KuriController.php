<?php

namespace App\Http\Controllers;

use App\Models\Kuri;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class KuriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Validation rules
        $rules = [
            'userName' => 'required|email|unique:kuris,userName',
            'password' => 'required|min:8',
            'confirm' => 'required|same:password',
            'name' => 'required',
            'contact' => 'required|numeric',
            'one_lot_amt' => 'required',
            'total_kuri_amt' => 'required',
            'address' => 'required',
            'no_of_lots' => 'required|numeric',
            'type' => 'required',
            'roganisor_lot' => 'required',
            'acceptTerms' => 'required|accepted',
        ];

        // Custom validation messages
        $messages = [
            'userName.unique' => 'Username already exists.',
            'confirm.same' => 'The confirmation password does not match.',
        ];

        // Validate the request data
        $validatedData = $request->validate($rules, $messages);

        $create_user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['userName'],
            'password' => bcrypt($validatedData['password'])

        ]);

        unset($validatedData['password']);

        $chitti = Kuri::create($validatedData);

        $tableName = str_replace(' ', '_', trim($request->input('name')));
        $numberOfColumns = $request->input('no_of_lots', 0);

        // Append a timestamp to make the table name unique
        $timestamp = now()->timestamp;
        $uniqueTableName = $tableName . '_' . $timestamp;

        if (!Schema::hasTable($uniqueTableName)) {
            Schema::create($uniqueTableName, function (Blueprint $table) use ($numberOfColumns) {
                $table->id();
                $table->string('member_name');
                $table->string('contact');
                $table->string('kuri_amount');
                for ($i = 1; $i <= $numberOfColumns; $i++) {
                    $columnName = 'lot_' . $i;
                    $table->timestamp($columnName)->nullable();
                    $payModeColumnName = 'pay_mode_' . $i;
                    $table->char($payModeColumnName, 1)->default('M');
                }
                $table->string('created_by');
                $table->unsignedBigInteger('kuri_id');

                $table->foreign('kuri_id')->references('id')->on('kuris');

                $table->timestamps();
            });
            $lastInsertedId = Kuri::latest('id')->first()->id;

            Kuri::where('id', $lastInsertedId)->update(['table_name' => $uniqueTableName]);

            return response()->json(['message' => 'Chitty created successfully.'], 201);
        } else {
            return response()->json(['message' => 'Chitty already exists. Please choose a different Chitty name.'], 400);
        }
    }
    public function addMember(Request $request)
    {

        // Validation rules
        $rules = [
            'name' => 'required',
            'contact' => 'required',
            'lot_number' => 'required',
            'kuri_amount' => 'required',
        ];

        // Custom validation messages
        $messages = [
            'name' => 'Member Name Required.',
            'lot_number' => 'Lot Number Required',
            'contact' => 'Member Contact Required',
            'kuri_amount' => 'Kuri Amount Required',
        ];

        $validatedData = $request->validate($rules, $messages);
        $table = Kuri::where('username', auth()->user()->email)->first(['table_name']);
        $table = $table->table_name;
        $kuri_id = Kuri::where('table_name', $table)->first(['id']);
        $kuri_id = $kuri_id->id;
        if ($table) {
            DB::table($table)->insert([
                'member_name' => $validatedData['name'],
                'contact' => $validatedData['contact'],
                 'lot_number' => $validatedData['lot_number'],
                'kuri_amount' => $validatedData['kuri_amount'],
                'created_by' => auth()->user()->email,
                'kuri_id' => $kuri_id,
            ]);

            return redirect()->route('dashboard')->with('success', 'Member added successfully.');
        } else {
            return redirect()->route('dashboard')->with('failed', 'Member added successfully.');
        }
    }

    public function mark_payment(Request $request)
    {
        $memId = $request->input('memId');
        $column = $request->input('lot_no');
        $colmn = trim(str_replace('lot_', '', $column));
        $selectedOption = $request->input('paymentOption');

        $kuri = Kuri::where('username', auth()->user()->email)->first(['table_name']);

        if (!$kuri) {
            return redirect()->route('view-kuri')->with('failed', 'Kuri not found.');
        }

        $table = $kuri->table_name;

        // Use the Eloquent model to update the record
        $record = DB::table($table)->where('id', $memId);

        $save = $record->update([
            $column => now(),
            'pay_mode_' . $colmn => $selectedOption
        ]);

        if ($save) {
            return redirect()->route('view-kuri')->with('success', 'Payment Recorded Successfully.');
        } else {
            return redirect()->route('view-kuri')->with('failed', 'Payment Recorded failed.');
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kuri  $kuri
     * @return \Illuminate\Http\Response
     */
    public function show(Kuri $kuri)
    {
        //
    }
    public function get_reports($lot_no)
    {
        $table = Kuri::where('username', auth()->user()->email)
            ->select('table_name', 'no_of_lots')
            ->first();

        $table_name = $table->table_name;
        $no_of_lots = $table->no_of_lots;


        if ($lot_no == "0") {
            $view_type="all";
            $data = DB::table($table_name)->get();
        } else {
            $data = DB::table($table_name)
                ->select('member_name', 'contact', 'lot_' . $lot_no, 'pay_mode_' . $lot_no,'kuri_amount')
                ->get();
                $view_type="individual";
        }

        return response()->json(['view_type' => $view_type,'data' => $data, 'no_of_lots' => $no_of_lots]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kuri  $kuri
     * @return \Illuminate\Http\Response
     */
    public function edit(Kuri $kuri)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kuri  $kuri
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kuri $kuri)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kuri  $kuri
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kuri $kuri)
    {
        //
    }
}
